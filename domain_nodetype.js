Drupal.behaviors.domainNodetype= function (context) {
  var disabled = $("#edit-domain-nodetype-0").attr("checked");
  var domains = $("#domain-nodetype-default-domains");

  if (disabled) {
    domains.css('display', 'none');
  }

  $("#edit-domain-nodetype-0").click(function() {domains.fadeOut("fast")});
  $("#edit-domain-nodetype-1").click(function() {domains.fadeIn("fast")});
};

